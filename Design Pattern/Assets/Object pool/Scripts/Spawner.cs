﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private float cooldown = 3f;

    void Update()
    {
        cooldown -= Time.deltaTime;
        if (cooldown < 0)
        {
            Pool.Instanse.Get("Enemy", new Vector2(Random.Range(-2.4f, 2.4f), 6f), 0);
            cooldown = 2f;
        }
    }
}