﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Return_to_pool : MonoBehaviour
{
    private int HP = 3;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            HP--;
            Pool.Instanse.Set(collision.tag);
            if (HP < 1)
            {
                Scene.states = Scene.States.Defeat;
                FindObjectOfType<Scene>().SwitchStates();
                HP = 3;
            }
        }
        else
        {
            Pool.Instanse.Set(collision.tag);
        }
    }
}