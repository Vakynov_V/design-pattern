﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    private Vector2 vector;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            vector = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Pool.Instanse.Get("Projectile", vector, 1000);
        }
    }
}