﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PoolObject
{
    public GameObject pref;
    public int amount;
}

public class Pool : MonoBehaviour
{
    public List<PoolObject> poolObjects;
    public List<GameObject> objectOnScene;
    [SerializeField, Header("Координаты родительского объекта")] Transform spawner;

    public static Pool Instanse;

    private void Awake()
    {
        if (Instanse == null)
        {
            Instanse = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        foreach (PoolObject item in poolObjects)
        {
            for(int i=0; i < item.amount; i++)
            {
                GameObject obj = Instantiate(item.pref, spawner);
                obj.SetActive(false);
                objectOnScene.Add(obj);
            }
        }
    }

    public GameObject Get(string tag, Vector2 vector, int Forse)
    {
        foreach (GameObject item in objectOnScene)
        {
            if (item.CompareTag(tag) && item.activeInHierarchy == false)
            {
                item.SetActive(true);
                item.transform.position = vector;
                item.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, Forse));
                return item;
            }
        }
        return null;
    }

    public GameObject Set(string tag)
    {
        foreach (GameObject item in objectOnScene)
        {
            if (item.CompareTag(tag) && item.activeInHierarchy == true)
            {
                item.SetActive(false);
                return item;
            }
        }return null;
    }
}