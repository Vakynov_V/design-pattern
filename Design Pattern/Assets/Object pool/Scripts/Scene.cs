﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scene : MonoBehaviour
{
    [SerializeField] private string startText;
    [SerializeField] private string defeatText;
    private Text text;
    private GameObject spawner;
    private GameObject screen;

    public enum States
    {
        Start,
        Gameplay,
        Defeat
    }

    public static States states;

    private void Start()
    {
        text = GetComponentInChildren<Text>();
        spawner = GameObject.Find("Spawner");
        screen = GameObject.Find("Screen/DeathZone");
        states = States.Start;
        SwitchStates();
    }

    private void Update()
    {
        if (Input.anyKeyDown)
        {
            if(states == States.Start)
            {
                states = States.Gameplay;
                SwitchStates();
            }
            else if(states == States.Defeat)
            {
                states = States.Start;
                SwitchStates();
            }
        }
    }

    public void SwitchStates()
    {
        switch (states)
        {
            case States.Start:
                text.text = startText;
                spawner.SetActive(false);
                screen.SetActive(false);
                break;

            case States.Gameplay:
                text.text = null;
                spawner.SetActive(true);
                screen.SetActive(true);
                break;

            case States.Defeat:
                text.text = defeatText;
                spawner.SetActive(false);
                screen.SetActive(false);
                break;
        }
    }
}