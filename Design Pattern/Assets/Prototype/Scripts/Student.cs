﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Student: ICloneable
{
    public string sername;
    public string name;
    //public int course;
    //public int scholarship;
    //public enum subjects {math, physics, oop, terVer, obg, history };
    public int age;//для скрипта "Clone"(Prototip_Clonirovanie)
    public Group group;//для скрипта "Clone"(Prototip_Clonirovanie)

    //public Dictionary<subjects, int> marks = new Dictionary<subjects, int>();

    public Student(string sername, string name, /*int course, int scholarship,*/ int age, Group group)
    {
        this.sername = sername;
        this.name = name;
        //this.course = course;
        //this.scholarship = scholarship;
        this.age = age;
        this.group = group;
    }

    public object Clone()
    {
        
        //return MemberwiseClone();
        Group group_X = new Group (group.course, group.spec);
        return new Student(sername, name, age, group_X);
    }
}