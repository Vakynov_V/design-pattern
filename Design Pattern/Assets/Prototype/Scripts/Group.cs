﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Group : MonoBehaviour
{
    public int course;
    public string spec;

    public Group (int course, string spec)
    {
        this.course = course;
        this.spec = spec;
    }
}
