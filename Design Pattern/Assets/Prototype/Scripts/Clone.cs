﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clone : MonoBehaviour
{
    private void Start()
    {
        Student student_0 = new Student("Amberman", "Anton", 18, new Group(1, "smth"));

        Student student_1 = (Student)student_0.Clone();
        
        student_1.group.course++;
        
        Debug.Log(student_0.group.course);
        Debug.Log(student_1.group.course);
    }
}