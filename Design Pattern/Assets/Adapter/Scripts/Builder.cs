﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Builder : MonoBehaviour
{
    public void Build()
    {
        Debug.Log("Строим дом");
    }

    public void GetSalary(Salary salary)
    {
        Debug.Log("Получено " + salary.money + " рублей");
    }
}