﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Company : MonoBehaviour
{
    public IWorker worker;

    void Start()
    {
        worker = new Adapter(new Builder());
        worker.Work();
        worker.PayMoney(30000);
    }
}