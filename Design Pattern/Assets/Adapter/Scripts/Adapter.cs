﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Adapter : IWorker
{
    public Builder builder;

    public Adapter (Builder builder)
    {
        this.builder = builder;
    }

    public void PayMoney(int v)
    {
        builder.GetSalary(new Salary(v));
    }

    public void Work()
    {
        builder.Build();
    }
}