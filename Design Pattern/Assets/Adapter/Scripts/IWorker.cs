﻿public interface IWorker 
{
    void Work();
    void PayMoney(int v);
}