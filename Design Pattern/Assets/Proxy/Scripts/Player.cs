﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameInfo Session;
    public Proxy proxy;
    public Proxy_2 proxy_2;

    public void Start()
    {
        proxy = new Proxy();
        proxy_2 = new Proxy_2();
        proxy.CompleteLvl();
        proxy_2.CompleteLvl();
    }
}