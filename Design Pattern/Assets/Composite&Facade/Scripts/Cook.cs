﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cook: MonoBehaviour
{
    public Pot pot;
    public Pan pan;
    public Knife knife;

    public void Start()
    {
        pot = new Pot();
        pan = new Pan();
        knife = new Knife();
    }
}