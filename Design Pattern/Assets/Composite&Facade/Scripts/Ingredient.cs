﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient
{
    public string name;
    protected float weight;
    protected float calorific;
    protected float cost;

    public Ingredient (string name, float weight, float calorific, float cost)
    {
        this.name = name;
        this.weight = weight;
        this.calorific = calorific;
        this.cost = cost;
    }

    public virtual void Cut()
    {

    }

    public virtual void Boil()
    {

    }

    public virtual void Fry()
    {

    }

    public virtual float GetTotalCost()
    {
        return 0;
    }

    public virtual float GetTotalWeight()
    {
        return 0;
    }

    public virtual float GetTotalCalories()
    {
        return 0;
    }

    public virtual void AddIngredient(Ingredient i)
    {

    }

    public virtual void RemoveIngredient(Ingredient i)
    {

    }
}