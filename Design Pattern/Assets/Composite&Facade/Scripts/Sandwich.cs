﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sandwich : MonoBehaviour
{
    
    void Start()
    {
        SimpleIngredient mayonnaise;
        mayonnaise = new SimpleIngredient("майонез", 100, 100, 100);

        SimpleIngredient ketchup;
        ketchup = new SimpleIngredient("кетчуп", 20, 10, 5);

        ComplexIngredient ketchonnaise;
        ketchonnaise = new ComplexIngredient("кетчонез", new List<Ingredient> { mayonnaise, ketchup });

        SimpleIngredient chees;
        chees = new SimpleIngredient("сыр", 150, 200, 300);

        SimpleIngredient sausage;
        sausage = new SimpleIngredient("колбаса", 300, 250, 250);

        SimpleIngredient bread;
        bread = new SimpleIngredient("хлеб", 100, 50, 5);

        ComplexIngredient sandwich;
        sandwich = new ComplexIngredient("сэндвич", new List<Ingredient> { bread, ketchonnaise, chees, sausage, bread });

        Debug.Log(sandwich.name);
        Debug.Log("Wheit " + sandwich.GetTotalWeight());
        Debug.Log("Colories " + sandwich.GetTotalCalories());
        Debug.Log("Cost " + sandwich.GetTotalCost());
    }
}