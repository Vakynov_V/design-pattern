﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplexIngredient : Ingredient
{
    private List<Ingredient> ingredients;

    public ComplexIngredient(string name, List<Ingredient> ingredients) : base(name, 0, 0, 0)
    {
        this.ingredients = ingredients;
    }

    public override void AddIngredient(Ingredient i)
    {
        ingredients.Add(i);
    }

    public override void Boil()
    {
        foreach (Ingredient ingredient in ingredients)
        {
            ingredient.Boil();
        }
    }

    public override void Cut()
    {
        foreach(Ingredient ingredient in ingredients)
        {
            ingredient.Cut();
        }
    }

    public override void Fry()
    {
        foreach(Ingredient ingredient in ingredients)
        {
            ingredient.Fry();
        }
    }

    public override float GetTotalCalories()
    {
        float totalCalories = 0;
        foreach (Ingredient ingredient in ingredients)
        {
            totalCalories += ingredient.GetTotalCalories();
        }
        return totalCalories;
    }

    public override float GetTotalCost()
    {
        float totalCost = 0;
        foreach (Ingredient ingredient in ingredients)
        {
            totalCost += ingredient.GetTotalCost();
        }
        return totalCost;
    }

    public override float GetTotalWeight()
    {
        float totalWeight = 0;
        foreach (Ingredient ingredient in ingredients)
        {
            totalWeight += ingredient.GetTotalWeight();
        }
        return totalWeight;
    }

    public override void RemoveIngredient(Ingredient i)
    {
        ingredients.Remove(i);
    }
}