﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleIngredient : Ingredient
{
    public SimpleIngredient(string name, float weight, float calorific, float cost): base (name, weight, calorific, cost)
    {

    }

    public override void AddIngredient(Ingredient i)
    {
        base.AddIngredient(i);
    }

    public override void Boil()
    {
        Debug.Log("Варим " + name);
    }

    public override void Cut()
    {
        Debug.Log("Режем " + name);
    }

    public override void Fry()
    {
        Debug.Log("Жарим " + name);
    }

    public override float GetTotalCalories()
    {
        float totalCalories = calorific * weight;
        return totalCalories;
    }

    public override float GetTotalCost()
    {
        float totalCost = weight * cost;
        return totalCost;
    }

    public override float GetTotalWeight()
    {
        return weight;
    }

    public override void RemoveIngredient(Ingredient i)
    {
        base.RemoveIngredient(i);
    }
}