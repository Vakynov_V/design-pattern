﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Realisation : MonoBehaviour
{
    public OverridenA a = new OverridenA();
    public OverridenB b = new OverridenB();
    void Start()
    {
        TemplateMethod();
    }

    public void TemplateMethod()
    {
        Debug.Log(a.TemplateMethod("", 0, 0, 0));
        Debug.Log(b.TemplateMethod("", 0, 0, 0));
    }
}