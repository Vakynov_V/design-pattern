﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverridenA : Abstract_Class
{
    public override int AbstractA(string s)
    {
        return s.Length;
    }

    public override int AbstractB()
    {
        return 42;
    }
}