﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Abstract_Class
{
    public abstract int AbstractA(string s);

    public abstract int AbstractB();

    public virtual int VirtualA(int a, int b)
    {
        return a + b;
    }

    public virtual int VirtualB(int c)
    {
        return (int)Mathf.Pow(c, 2);
    }

    public int TemplateMethod(string s, int a, int b, int c)
    {
        return AbstractA(s) ^ AbstractB() + VirtualA(a, b) * VirtualB(c);
    }
}