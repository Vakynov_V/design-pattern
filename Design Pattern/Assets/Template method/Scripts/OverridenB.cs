﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverridenB : Abstract_Class
{
    public override int AbstractA(string s)
    {
        char[] glasnie = new char[] { 'а', 'у', 'е', 'о', 'ю', 'э', 'и', 'я', 'ы', 'ё' };
        int sum = 0;
        for (int i = 0; i < s.Length; i++)
        {
            for(int j = 0; j < glasnie.Length; j++)
            {
                if (i == j)
                {
                    sum++;
                }
            }
        }
        return sum;
    }

    public override int AbstractB()
    {
        return Random.Range(1,3);
    }

    public override int VirtualA(int a, int b)
    {
        return a * b;
    }

    public override int VirtualB(int c)
    {
        return c % 7;
    }
}