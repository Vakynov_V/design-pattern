﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomDebugerDecorator : IDebugable
{
    public IDebugable debugable;

    public virtual void Show(string s)
    {
        debugable.Show(s);
    }

    public CustomDebugerDecorator(IDebugable debugable)
    {
        this.debugable = debugable;
    }
}