﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecoratorTest : MonoBehaviour
{
    void Start()
    {
        CustomDebuger cd = new CustomDebuger();
        SomeNewDecorator newDec = new SomeNewDecorator(cd);
        SomeExtraNewDecorator extraNewDec = new SomeExtraNewDecorator(cd);
    }
}