﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomeNewDecorator : CustomDebugerDecorator
{
    public SomeNewDecorator(IDebugable debugable) : base(debugable)
    {

    }

    public override void Show(string s)
    {
        base.Show(s);
        Debug.Log(s.Length);
    }
}